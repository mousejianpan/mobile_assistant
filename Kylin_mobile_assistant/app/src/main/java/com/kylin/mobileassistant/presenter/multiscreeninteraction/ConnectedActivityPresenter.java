package com.kylin.mobileassistant.presenter.multiscreeninteraction;

import com.kylin.mobileassistant.base.presenter.BasePresenter;
import com.kylin.mobileassistant.ui.activity.interfaces.multiscreeninteraction.IConnectedActivity;

public class ConnectedActivityPresenter extends BasePresenter<IConnectedActivity> {
}
