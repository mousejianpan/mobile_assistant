package com.kylin.mobileassistant.presenter.main;

import com.kylin.mobileassistant.base.presenter.BasePresenter;
import com.kylin.mobileassistant.ui.activity.interfaces.main.IMainActivityView;

/**
 * @author : mechrevo
 * @date : 2021/12/10 15:55
 */
public class MainActivityPresenter extends BasePresenter<IMainActivityView> {

    public void handleSomeData() {
        getView().showAppToast("哈哈哈哈哈哈哈哈哈", 0);
        getView().showLoading("干啥呢");
    }

}
