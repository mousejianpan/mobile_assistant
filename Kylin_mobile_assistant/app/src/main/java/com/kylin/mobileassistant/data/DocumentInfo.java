package com.kylin.mobileassistant.data;

public class DocumentInfo extends BaseInfo {
    public DocumentInfo(String path, String name, long size) {
        super(path, name, size);
    }
}
