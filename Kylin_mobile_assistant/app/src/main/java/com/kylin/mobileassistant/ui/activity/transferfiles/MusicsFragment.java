package com.kylin.mobileassistant.ui.activity.transferfiles;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.base.view.BaseMvpFragment;
import com.kylin.mobileassistant.presenter.transferfiles.MusicsFragmentPresenter;
import com.kylin.mobileassistant.ui.activity.interfaces.transferfiles.IMusicsFragment;

public class MusicsFragment extends BaseMvpFragment<MusicsFragmentPresenter>
        implements IMusicsFragment {
    private TransferFilesActivity parent = null;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.loadMusicView(view, parent);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        parent = (TransferFilesActivity)context;
    }

    @Override
    public void onResume() {
        mPresenter.updateView(parent);
        super.onResume();
    }

    @Override
    protected void initPresenter() {
        mPresenter = new MusicsFragmentPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_send_musics;
    }
}
