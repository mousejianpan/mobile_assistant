package com.kylin.mobileassistant.presenter.transferfiles;

import android.database.Cursor;
import android.provider.MediaStore;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.adapter.DocumentListAdapter;
import com.kylin.mobileassistant.base.presenter.BasePresenter;
import com.kylin.mobileassistant.data.DocumentInfo;
import com.kylin.mobileassistant.ui.activity.interfaces.transferfiles.IDocumentsFragment;
import com.kylin.mobileassistant.ui.activity.transferfiles.TransferFilesActivity;

import java.util.ArrayList;

public class DocumentsFragmentPresenter extends BasePresenter<IDocumentsFragment> {
    private ArrayList<DocumentInfo> documentInfos = new ArrayList<>();
    private DocumentListAdapter documentListAdapter = null;

    public void loadDocumentsView(View view, TransferFilesActivity transferFilesActivity) {
        getDocumentsInfo(view);
        documentListAdapter = new DocumentListAdapter(view.getContext(), R.layout.piece_document, documentInfos);
        ListView listView = view.findViewById(R.id.documents_view);
        listView.setAdapter(documentListAdapter);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if(scrollState == SCROLL_STATE_IDLE) {
                    documentListAdapter.notifyDataSetChanged();
                    transferFilesActivity.setViewPagerSlide(true);
                } else {
                    transferFilesActivity.setViewPagerSlide(false);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                documentListAdapter.setFirstVisible(firstVisibleItem);
                documentListAdapter.setVisibleNum(visibleItemCount);
            }
        });
        listView.setOnItemClickListener((parent, view1, position, id) -> {
            View child = listView.getChildAt(position - documentListAdapter.getFirstVisible());
            ImageView imageView = child.findViewById(R.id.check_box_3);
            if(documentListAdapter.isContained(position)) {
                documentListAdapter.unSelecting(position);
                imageView.setImageBitmap(TransferFilesActivityPresenter.getUncheckedIcon());
            } else {
                documentListAdapter.selecting(position);
                imageView.setImageBitmap(TransferFilesActivityPresenter.getCheckedIcon());
            }
            TextView selectedNumView = transferFilesActivity.findViewById(R.id.text_select);
            String text = "已选择" + documentListAdapter.size() + "项";
            selectedNumView.setText(text);
        });
    }

    public void getDocumentsInfo(View view) {
        String[] projection = new String[] {
                MediaStore.Files.FileColumns.DISPLAY_NAME,
                MediaStore.Files.FileColumns.SIZE,
                MediaStore.Files.FileColumns.DATA,
        };
        String type = MediaStore.Files.FileColumns.MIME_TYPE;
        String selection = type + "=? or " + type + "=? or " + type + "=? or " +
                        type + "=? or " + type + "=? or " + type + "=? or " +
                        type + "=? or " + type + "=? or " + type + "=?" ;
        String[] selectionArgs = new String[] {
                MimeTypeMap.getSingleton().getMimeTypeFromExtension("pdf"),
                MimeTypeMap.getSingleton().getMimeTypeFromExtension("ppt"),
                MimeTypeMap.getSingleton().getMimeTypeFromExtension("pptx"),
                MimeTypeMap.getSingleton().getMimeTypeFromExtension("doc"),
                MimeTypeMap.getSingleton().getMimeTypeFromExtension("docx"),
                MimeTypeMap.getSingleton().getMimeTypeFromExtension("xls"),
                MimeTypeMap.getSingleton().getMimeTypeFromExtension("xlsx"),
                MimeTypeMap.getSingleton().getMimeTypeFromExtension("txt"),
                MimeTypeMap.getSingleton().getMimeTypeFromExtension("zip")
        };
        Cursor cursor = view.getContext().getContentResolver().query(
                MediaStore.Files.getContentUri("external"),
                projection, selection, selectionArgs,
                MediaStore.Files.FileColumns.MIME_TYPE);
        cursor.moveToFirst();
        int nameIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.DISPLAY_NAME);
        int pathIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.DATA);
        int sizeIndex = cursor.getColumnIndex(MediaStore.Files.FileColumns.SIZE);
        while(cursor.moveToNext()) {
            long size = cursor.getLong(sizeIndex);
            String name = cursor.getString(nameIndex);
            String path = cursor.getString(pathIndex);
            if(size > 0) {
                documentInfos.add(new DocumentInfo(path, name, size));
            }
        }
        cursor.close();
    }
}
