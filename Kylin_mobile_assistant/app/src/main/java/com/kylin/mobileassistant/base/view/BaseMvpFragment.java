package com.kylin.mobileassistant.base.view;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.kylin.mobileassistant.base.interfaces.IBaseContract;
import com.kylin.mobileassistant.base.presenter.BasePresenter;
import com.kylin.mobileassistant.base.presenter.PresenterManager;
import com.kylin.mobileassistant.utils.DisplayUtils;
import com.kylin.mobileassistant.utils.ToastUtil;
import com.kylin.mobileassistant.view.LoadingDialog;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * @author zhaoyang
 * @date 2020/7/30
 * MVP 架构的fragment的基类，如果不使用databinding继承此类，否则继承BaseMvpBindingFragment)
 */
public abstract class BaseMvpFragment<T extends BasePresenter> extends AbstractBaseFragment
        implements IBaseContract.IBaseMvpView {

    protected T mPresenter;

    protected PresenterManager mPresenterManager;

    private LoadingDialog mLoadingDialog;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPresenter();
        mPresenterManager = new PresenterManager();
        if (savedInstanceState != null && mPresenterManager.containsPresenter(savedInstanceState)) {
            mPresenter = mPresenterManager.<T>restorePresenter(savedInstanceState);
        }
    }

    /**
     * 初始化presenter
     */
    protected abstract void initPresenter();

    /**
     * Attach view to presenter.
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mPresenter != null) {
            mPresenter.attachView(this);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mPresenter != null) {
            mPresenter.onViewStart();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        DisplayUtils.hideSoftKeyboardForView(getActivity(), getView());
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mPresenter != null) {
            mPresenter.onViewStop();
        }
    }

    /**
     * Save presenter using presenter manager.
     *
     * @param outState out state of saved instance
     */
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        if (mPresenter != null) {
            mPresenterManager.savePresenter(mPresenter, outState);
        }
    }

    /**
     * Detach view from presenter.
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPresenter != null) {
            mPresenter.detachView();
        }
    }

    /**
     *
     * @return presenter实例
     */
    public T getPresenter() {
        return mPresenter;
    }

    /**
     * About crash that'Fatal Exception: java.lang.IllegalStateException Can not perform this action after onSaveInstanceState'.
     * Due to this crash often occurs on Fabric. To avoid it, added processing logic.
     *
//     * @param fragmentManager
//     * @param fragmentTransaction
     */
    protected void handleFragmentForCommit(FragmentManager fragmentManager, FragmentTransaction fragmentTransaction) {
        if (fragmentManager.isStateSaved()) {
            fragmentTransaction.commitAllowingStateLoss();
        } else {
            fragmentTransaction.commit();
        }
    }

    @Override
    public void showLoading() {
        try {
            if (mLoadingDialog != null) {
                mLoadingDialog.dismiss();
            }
            if (getBaseActivity() != null) {
                mLoadingDialog = new LoadingDialog(getBaseActivity(), "加载中", true);
                mLoadingDialog.show();
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showLoading(String content, boolean cancelable) {
        try {
            if (mLoadingDialog != null) {
                mLoadingDialog.dismiss();
            }
            if (getBaseActivity() != null) {
                mLoadingDialog = new LoadingDialog(getActivity(), content, cancelable);
                mLoadingDialog.show();
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showLoading(boolean cancelable) {
        if (mLoadingDialog != null) {
            mLoadingDialog.dismiss();
        }
        if (getBaseActivity() != null) {
            mLoadingDialog = new LoadingDialog(getActivity(), "加载中", cancelable);
            mLoadingDialog.show();
        }
    }

    @Override
    public void showLoading(String content) {
        try {
            if (mLoadingDialog != null) {
                mLoadingDialog.dismiss();
            }
            if (getBaseActivity() != null) {
                mLoadingDialog = new LoadingDialog(getActivity(), content, true);
                mLoadingDialog.show();
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stopLoading() {
        try {
            if (mLoadingDialog != null) {
                mLoadingDialog.dismiss();
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void showBusinessToast(String content, int type) {
        if (getBaseActivity() != null) {
            ToastUtil.showBottom(getActivity(), content, type);
        }
    }

    @Override
    public void showAppToast(String content, int type) {
        if (getBaseActivity() != null) {
            ToastUtil.showBottom(getActivity().getApplication(), content, type);
        }
    }

    @Override
    public void hideInput() {
        if (getBaseActivity() != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            View v = getActivity().getWindow().peekDecorView();
            if (null != v) {
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        }
    }

    @Override
    public void showInput(View et) {
        et.requestFocus();
        if (getBaseActivity() != null) {
            InputMethodManager imm = (InputMethodManager)
                    getActivity().getSystemService(INPUT_METHOD_SERVICE);
            imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
        }
    }

}
