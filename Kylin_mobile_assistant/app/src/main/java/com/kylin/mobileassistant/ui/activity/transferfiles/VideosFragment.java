package com.kylin.mobileassistant.ui.activity.transferfiles;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.base.view.BaseMvpFragment;
import com.kylin.mobileassistant.presenter.transferfiles.VideosFragmentPresenter;
import com.kylin.mobileassistant.ui.activity.interfaces.transferfiles.IVideosFragment;

public class VideosFragment extends BaseMvpFragment<VideosFragmentPresenter>
        implements IVideosFragment {
    TransferFilesActivity parent = null;

    @Override
    protected void initPresenter() {
        mPresenter = new VideosFragmentPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_send_videos;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        parent = (TransferFilesActivity)context;
    }

    @Override
    public void onResume() {
        mPresenter.updateView(parent);
        super.onResume();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.loadVideoView(view, parent);
    }
}
