package com.kylin.mobileassistant.ui.activity.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.base.view.BaseMvpFragment;
import com.kylin.mobileassistant.presenter.main.MultiScreenInteractionFragmentPresenter;
import com.kylin.mobileassistant.ui.activity.interfaces.main.IMultiScreenInteractionFragment;
import com.kylin.mobileassistant.ui.activity.multiscreeninteraction.ConnectedActivity;

public class MultiScreenInteractionFragment extends BaseMvpFragment<MultiScreenInteractionFragmentPresenter>
        implements IMultiScreenInteractionFragment {
    private MainActivity parent = null;

    @Override
    public void onAttach(@NonNull Context context) {
        parent = (MainActivity)context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflateView(inflater, container);
        view.findViewById(R.id.connect_to_computer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), ConnectedActivity.class);
                startActivity(intent);
            }
        });
        return view;
    }

    @Override
    protected void initPresenter() {
        mPresenter = new MultiScreenInteractionFragmentPresenter();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_multi_screen_interaction;
    }

    @Override
    public void onResume() {
        TextView textView = parent.findViewById(R.id.top_text);
        textView.setText("投屏互动");
        super.onResume();
    }
}
