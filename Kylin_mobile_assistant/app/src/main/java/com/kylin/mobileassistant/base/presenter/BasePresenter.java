package com.kylin.mobileassistant.base.presenter;

import androidx.annotation.Nullable;

import com.kylin.mobileassistant.base.interfaces.IBaseContract;
import com.kylin.mobileassistant.base.view.AbstractBaseFragment;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * @author : zhaoyang
 * @date : 2020/7/30
 * presenter基类
 */
public abstract class BasePresenter<T extends IBaseContract.IBaseMvpView> implements IBaseContract.IPresenter<T> {

    private T mMvpView;

    /**
     * disposable管理器，用于清理disposable
     */
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();

    public BasePresenter() {

    }

    @Override
    public void attachView(T mvpView) {
        mMvpView = mvpView;
        //addSubscription(Disposable);

    }

    @Override
    public void detachView() {
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
        }
        mMvpView = null;
    }

    /**
     * 添加disposable
     *
     * @param d disposable
     */
    protected void addSubscription(Disposable d) {
        mCompositeDisposable.add(d);
    }

    /**
     * 移除disposable
     *
     * @param d disposable
     */
    protected void removeSubscription(Disposable d) {
        mCompositeDisposable.remove(d);
        if (!d.isDisposed()) {
            d.dispose();
        }
    }

    protected boolean isViewAttached() {
        return mMvpView != null;
    }

    @Nullable
    protected T getView() {
        return mMvpView;
    }

    /**
     * 视图是否可见
     *
     * @return true 可见， false 不可见
     */
    protected boolean isViewVisible() {
        return getView() != null && getView() instanceof AbstractBaseFragment && ((AbstractBaseFragment) getView()).isFragmentVisible();
    }

    /**
     * onStart时的回调
     */
    public void onViewStart() {

    }

    /**
     * onStop时回调
     */
    public void onViewStop() {

    }

}
