package com.kylin.mobileassistant.base.interfaces;

import android.view.View;

/**
 * @author : zhaoyang
 * @date : 2020/7/31
 */
public interface IBaseContract {

    interface IBaseMvpView {

        /**
         * 显示加载弹窗
         *
         * @param content 加载弹窗文字
         */
        void showLoading(String content);
        void showLoading(String content, boolean cancelable);

        /**
         * 显示加载弹窗
         */
        void showLoading();
        void showLoading(boolean cancelable);

        /**
         * 隐藏加载弹窗
         */
        void stopLoading();

        /**
         * 传视图context，toast显示依赖于视图生命周期
         *
         * @param content 视图context
         * @param type    0 普通toast，1 成功toast，2 失败toast，3 警告toast
         */
        void showBusinessToast(String content, int type);

        void hideInput();

        void showInput(View view);

        /**
         * 传app的context，toast与视图生命周期无关
         *
         * @param content app的contex
         * @param type    0 普通toast，1 成功toast，2 失败toast，3 警告toast
         */
        void showAppToast(String content, int type);

    }

    interface IPresenter<V extends IBaseMvpView> {

        void attachView(V mvpView);

        void detachView();
    }
}
