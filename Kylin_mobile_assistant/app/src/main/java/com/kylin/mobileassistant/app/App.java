package com.kylin.mobileassistant.app;

import android.app.Application;

/**
 * @author : mechrevo
 * @date : 2021/12/10 14:43
 */
public class App extends Application {

    private static App mInstance;

    public App() {
        mInstance = this;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static App getInstance() {
        return mInstance;
    }

}
