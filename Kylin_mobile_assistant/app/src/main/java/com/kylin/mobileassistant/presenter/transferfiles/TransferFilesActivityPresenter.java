package com.kylin.mobileassistant.presenter.transferfiles;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;

import androidx.core.content.res.ResourcesCompat;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.base.presenter.BasePresenter;
import com.kylin.mobileassistant.ui.activity.interfaces.transferfiles.ITransferFilesActivity;

public class TransferFilesActivityPresenter  extends BasePresenter<ITransferFilesActivity> {
    private static Bitmap uncheckedIcon = null;
    private static Bitmap checkedIcon = null;

    public void loadIcons(View view) {
        if(uncheckedIcon != null) return;
        Drawable drawable = ResourcesCompat.getDrawable(view.getContext().getResources(),
                R.drawable.check_box_1, null);
        assert drawable != null;
        uncheckedIcon = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(uncheckedIcon);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        Drawable drawable1 = ResourcesCompat.getDrawable(view.getContext().getResources(),
                R.drawable.check_box_2, null);
        assert drawable1 != null;
        checkedIcon = Bitmap.createBitmap(drawable1.getIntrinsicWidth(),
                drawable1.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas1 = new Canvas(checkedIcon);
        drawable1.setBounds(0, 0, canvas1.getWidth(), canvas1.getHeight());
        drawable1.draw(canvas1);
    }
    public static Bitmap getCheckedIcon() { return checkedIcon; }
    public static Bitmap getUncheckedIcon() { return uncheckedIcon; }
}
