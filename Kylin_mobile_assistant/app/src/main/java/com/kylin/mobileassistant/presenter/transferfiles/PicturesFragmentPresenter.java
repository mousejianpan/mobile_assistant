package com.kylin.mobileassistant.presenter.transferfiles;

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.kylin.mobileassistant.R;
import com.kylin.mobileassistant.adapter.PictureListAdapter;
import com.kylin.mobileassistant.base.presenter.BasePresenter;
import com.kylin.mobileassistant.data.PictureInfo;
import com.kylin.mobileassistant.ui.activity.interfaces.transferfiles.IPicturesFragment;
import com.kylin.mobileassistant.ui.activity.transferfiles.TransferFilesActivity;

import java.util.ArrayList;

public class PicturesFragmentPresenter extends BasePresenter<IPicturesFragment> {
    private final ArrayList<PictureInfo> pictures = new ArrayList<>();
    private PictureListAdapter pictureListAdapter = null;


    public void getPicturesInfo(View view) {
        String[] projection = new String[]{
                MediaStore.Images.Media.DATA,
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Media.SIZE,
                MediaStore.Images.Media.DATE_MODIFIED
        };
        Cursor cursor = view.getContext().getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection, null, null,
                MediaStore.Images.Media.DATE_MODIFIED + " DESC");
        pictures.clear();
        int dataIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
        int thumbIndex = cursor.getColumnIndex(MediaStore.Images.ImageColumns._ID);
        int nameIndex = cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME);
        int sizeIndex = cursor.getColumnIndex(MediaStore.Images.Media.SIZE);
        for(cursor.moveToFirst(); cursor.moveToNext();) {
            String path = cursor.getString(dataIndex);
            long thumpId = cursor.getLong(thumbIndex);
            Uri uri = ContentUris.withAppendedId(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, thumpId);
            String name = cursor.getString(nameIndex);
            long size = cursor.getLong(sizeIndex);
            if(size != 0) {
                pictures.add(new PictureInfo(path, name, uri, size));
            }
            cursor.moveToNext();
        }
        cursor.close();
    }

    public void updateView(TransferFilesActivity transferFilesActivity) {
        TextView selectedNumView = transferFilesActivity.findViewById(R.id.text_select);
        String text = "已选择" + pictureListAdapter.size() + "项";
        selectedNumView.setText(text);
        pictureListAdapter.notifyDataSetChanged();
    }

    public void loadPictureView(View view, TransferFilesActivity transferFilesActivity) {
        getPicturesInfo(view);
        pictureListAdapter = new PictureListAdapter(view.getContext(), R.layout.piece_picture, pictures);
        GridView gridView = view.findViewById(R.id.pictures_view);
        gridView.setAdapter(pictureListAdapter);
        gridView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if(scrollState == SCROLL_STATE_IDLE) {
                    pictureListAdapter.notifyDataSetChanged();
                    transferFilesActivity.setViewPagerSlide(true);
                } else {
                    transferFilesActivity.setViewPagerSlide(false);
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                pictureListAdapter.setFirstVisible(firstVisibleItem);
                pictureListAdapter.setVisibleNum(visibleItemCount);
            }
        });
        gridView.setOnItemClickListener((parent, view1, position, id) -> {
            View child = gridView.getChildAt(position - pictureListAdapter.getFirstVisible());
            ImageView imageView = child.findViewById(R.id.check_box_2);
            if(pictureListAdapter.isContained(position)) {
                pictureListAdapter.unSelecting(position);
                imageView.setImageBitmap(TransferFilesActivityPresenter.getUncheckedIcon());
            } else {
                pictureListAdapter.selecting(position);
                imageView.setImageBitmap(TransferFilesActivityPresenter.getCheckedIcon());
            }
            TextView selectedNumView = transferFilesActivity.findViewById(R.id.text_select);
            String text = "已选择" + pictureListAdapter.size() + "项";
            selectedNumView.setText(text);
        });
    }
}
